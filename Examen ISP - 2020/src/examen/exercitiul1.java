package examen;

class B extends A
{	
	private String param;
	private E e;
	public B()
	{
		
	}
	public B(E e)
	{
		this.e = e;
	}
}

class A
{	
	
}

class Z
{	
	public void g()
	{
		
	}
}

class E
{	
	
}

class C
{
	private B b = new B();
	public C()
	{
		// sau aici cu b = new(B)
	}
}

class D
{
	private B b;
	public D()
	{
		
	}
	public void setB(B b)
	{
		this.b = b;
	}
	public void f()
	{
		
	}
}
