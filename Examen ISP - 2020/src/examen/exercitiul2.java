package examen;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

class Interfata extends JFrame
{
	
	JFrame interfata = new JFrame();
	JButton buton = new JButton();
	static JTextField text1 = new JTextField();
	static JTextField text2 = new JTextField();
	
	Interfata()
	{
		
		interfata.setTitle("Exercitiul 2");
		interfata.setSize(500,300);
		interfata.setDefaultCloseOperation(interfata.EXIT_ON_CLOSE);
		interfata.setLayout(null);
		interfata.setVisible(true);
		text1.setBounds(20,20,400,40);
		interfata.add(text1);
		
		text2.setBounds(20,100,400,40);
		text2.setEditable(false);
		interfata.add(text2);
		
		buton.setText("Transfer");
		buton.setBounds(20,180,400,40);
		buton.addActionListener(new TratareButon());
		interfata.add(buton);
		
	}
	
}

class TratareButon implements ActionListener
{

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		String cuvant = Interfata.text1.getText();
		Interfata.text2.setText(cuvant);
		Interfata.text1.setText(" ");
		
	}
}

public class exercitiul2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Interfata interf = new Interfata();
		
	}

}
